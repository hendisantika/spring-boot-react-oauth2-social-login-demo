package com.hendisantika.springbootreactoauth2sociallogindemo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootReactOauth2SocialLoginDemoApplicationTests {

    @Test
    public void contextLoads() {
    }

}
