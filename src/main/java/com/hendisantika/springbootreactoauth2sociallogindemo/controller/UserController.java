package com.hendisantika.springbootreactoauth2sociallogindemo.controller;

import com.hendisantika.springbootreactoauth2sociallogindemo.entity.User;
import com.hendisantika.springbootreactoauth2sociallogindemo.exception.ResourceNotFoundException;
import com.hendisantika.springbootreactoauth2sociallogindemo.repository.UserRepository;
import com.hendisantika.springbootreactoauth2sociallogindemo.security.CurrentUser;
import com.hendisantika.springbootreactoauth2sociallogindemo.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-react-oauth2-social-login-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-11-29
 * Time: 07:14
 * To change this template use File | Settings | File Templates.
 */
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
    }
}